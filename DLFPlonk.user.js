// ==UserScript==
// @name        DLFPlonk
// @namespace   DLFPlonk
// @include     https://linuxfr.org/*
// @version     9
// @downloadURL https://framagit.org/peetah/DLFPlonk/raw/master/DLFPlonk.user.js
// @updateURL https://framagit.org/peetah/DLFPlonk/raw/master/DLFPlonk.user.js
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// ==/UserScript==

//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//  0. You just DO WHAT THE FUCK YOU WANT TO.
//

(function() {
    let userRE = new RegExp("https://linuxfr.org/users/(.*)$");
    let types = {
      article: 1,
      comment: 2
    };

    let plonkList = JSON.parse(GM_getValue("plonkList","[]"));
    switch(typeof plonkList){
    case "string":
      // check for pre-JSON configuration data
      plonkList = plonkList.split(',');
      if(plonkList.length === 1 && plonkList[0] === ""){
        plonkList = {};
      }
      // no break on purpose
    case "object":
      // check for pre-hash configuration data
      if(Array.isArray(plonkList)){
        plonkList = plonkList.reduce(function(obj,user,index){
          obj[user]=['',true,true];
          return obj;
        },{});
      }else{
        // check for configuration with lesser user specific options
        plonkList = Object.keys(plonkList).reduce(function(obj,user,index){
           switch(plonkList[user].length){
           case 1: obj[user]=[plonkList[user][0],true]; break;
           case 2: obj[user]=['',plonkList[user][0],plonkList[1]]; break;
           default: obj[user]=plonkList[user];break;
           }
          return obj;
        },{});
      }
      break;
    default:
      plonkList = {};
      break;
    }

    let plonkClosedAccounts = GM_getValue("plonkClosedAccounts",true);
    let unlistClosedAccounts = GM_getValue("unlistClosedAccounts",true);

    GM_addStyle(".overlaybg{position:fixed;top:0;left:0;width: 100%;height: 100%;background-color:#000; opacity:0.5;z-index:100}");
    GM_addStyle(".overlaywin{position:fixed;left:0;right:0;top:0;bottom:0;width:600px;height:480px;margin:auto;background-color:#FFF;z-index:101;padding:10px 20px;border:thick double #000}");
    GM_addStyle("#plonkList{position:relative;padding-top:25px;background:gray}");
    GM_addStyle("#plonkList>div{overflow-y:auto;height:190px;background:#ddd}");
    GM_addStyle("#plonkList table{width:100%;border-collapse:collapse}");
    GM_addStyle("#plonkList td, #plonkForm th{color:#000;text-align:center;border:1px;white-space:nowrap;padding:5px 25px;}");
    GM_addStyle("#plonkList th{height:0;line-height:0;padding-top:0;padding-bottom:0;border:none;color:transparent;white-space:nowrap}");
    GM_addStyle("#plonkList th>div,#plonkList th>input{position:absolute;top:0;background:transparent;color:#000;padding:4px 25px;margin-left:-6px;line-height:normal;}");
    GM_addStyle("#plonkForm button{background-image:none;padding-left:5px}");
    GM_addStyle("#plonkList+button{margin-top:10px}");
    GM_addStyle("#plonkForm h2{text-align:center}");

    function togglePlonk(user,type){
      plonkList[user][types[type]] = !plonkList[user][types[type]];
      GM_setValue("plonkList",JSON.stringify(plonkList));
    }

    function closePlonkList(){
      document.getElementsByClassName("overlaywin").item(0).remove();
      document.getElementsByClassName("overlaybg").item(0).remove();
      location.reload();
    }

    function buildForm(form,user,alias){
      let h2 = document.createElement("h2");
      h2.appendChild(document.createTextNode("OPTIONS"));
      form.appendChild(h2);

      let checkbox = document.createElement("input");
      checkbox.setAttribute("type","checkbox");
      checkbox.setAttribute("id","closedAccounts");
      checkbox.addEventListener("change",function(){
        plonkClosedAccounts = !plonkClosedAccounts;
        GM_setValue("plonkClosedAccounts",plonkClosedAccounts);
      });
      if(plonkClosedAccounts){
        checkbox.setAttribute("checked","checked");
      }
      let label = document.createElement("label");
      label.appendChild(checkbox);
      label.appendChild(
        document.createTextNode("Plonker les comptes désactivés.")
      );
      let div = document.createElement("div");
      div.appendChild(label);
      form.appendChild(div);

      checkbox = document.createElement("input");
      checkbox.setAttribute("type","checkbox");
      checkbox.setAttribute("id","unlistClosedAccounts");
      checkbox.addEventListener("change",function(){
        unlistClosedAccounts = !unlistClosedAccounts;
        GM_setValue("unlistClosedAccounts",unlistClosedAccounts);
      });
      if(unlistClosedAccounts){
        checkbox.setAttribute("checked","checked");
      }
      label = document.createElement("label");
      label.appendChild(checkbox);
      label.appendChild(
        document.createTextNode("Supprimer de la liste les comptes désactivés.")
      );
      div = document.createElement("div");
      div.appendChild(label);
      form.appendChild(div);

      h2 = document.createElement("h2");
      h2.appendChild(document.createTextNode("LISTE DE PLONKAGE"));
      form.appendChild(h2);

      let button = document.createElement("button");
      button.setAttribute("id","plonkAdd");
      button.appendChild(document.createTextNode("Ajouter '"+user+"'"));
      if( user !== alias.toLowerCase() ){
        button.appendChild(document.createTextNode("' alias '"+alias+"'"));
      }
      button.addEventListener("click",plonk.bind(null,user,alias),true);


      let container = document.createElement("div");
      container.setAttribute("id","plonkList");
      let list = document.createElement("div");
      container.appendChild(list);
      let table = document.createElement("table");
      list.appendChild(table);
      let thead = document.createElement("thead");
      table.appendChild(thead);
      let row = document.createElement("tr");
      thead.appendChild(row);
      let col = document.createElement("th");
      row.appendChild(col);
      checkbox = document.createElement("input");
      checkbox.setAttribute("type","checkbox");
      checkbox.addEventListener("change",function(){
        let users = document.getElementsByName("users");
        for (let i = 0; i < users.length ; i++){
          users[i].checked = !users[i].checked;
        }
      });
      col.appendChild(checkbox);
      col = document.createElement("th");
      row.appendChild(col);
      col.appendChild(document.createTextNode("Comptes"));
      div = document.createElement("div");
      div.appendChild(document.createTextNode("Comptes"));
      col.appendChild(div);
      col = document.createElement("th");
      row.appendChild(col);
      col.appendChild(document.createTextNode("Contenus"));
      div = document.createElement("div");
      div.appendChild(document.createTextNode("Contenus"));
      col.appendChild(div);
      col = document.createElement("th");
      row.appendChild(col);
      col.appendChild(document.createTextNode("Commentaires"));
      div = document.createElement("div");
      div.appendChild(document.createTextNode("Commentaires"));
      col.appendChild(div);

      let tbody = document.createElement("tbody");
      table.appendChild(tbody);
      if(Object.keys(plonkList).length){
        form.appendChild(button);
        if(plonkList.hasOwnProperty(user)){
          button.style.visibility = "hidden";
        }
        for(let u in plonkList){
          row = document.createElement("tr");
          row.setAttribute("id",u);

          col=document.createElement("td");
          checkbox = document.createElement("input");
          checkbox.setAttribute("id","user_"+u);
          checkbox.setAttribute("type","checkbox");
          checkbox.setAttribute("name","users");
          checkbox.setAttribute("value",u);
          col.appendChild(checkbox);
          row.appendChild(col);

          col=document.createElement("td");
          label = document.createElement("label");
          label.setAttribute("for","user_"+u);
          label.appendChild(document.createTextNode(u));
          if(plonkList[u][0] !== ''){
            label.appendChild(document.createElement("br"));
            label.appendChild(document.createTextNode("["+plonkList[u][0]+"]"));
          }
          col.appendChild(label);
          row.appendChild(col);

          col=document.createElement("td");
          checkbox = document.createElement("input");
          checkbox.setAttribute("type","checkbox");
          checkbox.setAttribute("name","articles_"+u);
          checkbox.addEventListener("change",togglePlonk.bind(null,u,'article'));
          if(plonkList[u][1]){
            checkbox.setAttribute("checked","checked");
          }
          col.appendChild(checkbox);
          row.appendChild(col);

          col=document.createElement("td");
          checkbox = document.createElement("input");
          checkbox.setAttribute("type","checkbox");
          checkbox.setAttribute("name","comments_"+u);
          checkbox.addEventListener("change",togglePlonk.bind(null,u,'comment'));
          if(plonkList[u][2]){
            checkbox.setAttribute("checked","checked");
          }
          col.appendChild(checkbox);
          row.appendChild(col);

          tbody.appendChild(row);
        }
      }else{
        let p = document.createElement("p");
        p.appendChild(
          document.createTextNode("Aucun utilisateur actif plonké pour le moment.")
        );
        form.appendChild(p);
        form.appendChild(button);
        container.remove();
        container = null;
      }

      if(container){
        form.appendChild(container);
      }

      button = document.createElement("button");
      button.appendChild(document.createTextNode("Supprimer la sélection"));
      button.addEventListener("click",function(e){
        unplonk(user,alias);
        e.preventDefault();
      },true);
      if(Object.keys(plonkList).length){
        form.appendChild(button);
      }

      button = document.createElement("button");
      button.appendChild(document.createTextNode("Fermer"));
      button.addEventListener("click",function(e){
        closePlonkList();
        e.preventDefault();
      },true);
      form.appendChild(button);
    }

    function plonk(user,alias){
      if(user === alias.toLowerCase()){
        storedAlias = '';
      }else{
        storedAlias = alias;
      }
      plonkList[user] = [storedAlias,true,true];
      GM_setValue("plonkList",JSON.stringify(plonkList));
      let form = document.getElementById("plonkForm");
      form.remove();
      form = document.createElement("form");
      form.setAttribute("id","plonkForm");
      buildForm(form,user,alias);
      document.getElementsByClassName("overlaywin").item(0).appendChild(form);
    }

    function unplonk(user,alias){
      let users = document.getElementsByName("users");
      // walk select options in reverse order to avoid
      // indices reordering consequence of option removal
      for (let i = users.length-1; i >= 0 ; i--){
        if(!users[i].checked){
          continue;
        }
        delete plonkList[users[i].value];
        document.getElementById(users[i].value).remove();
      }
      if(!Object.keys(plonkList).length){
        let form = document.getElementById("plonkForm");
        form.remove();
        form = document.createElement("form");
        form.setAttribute("id","plonkForm");
        buildForm(form,user,alias);
        document.getElementsByClassName("overlaywin").item(0).appendChild(form);
      }

      GM_setValue("plonkList",JSON.stringify(plonkList));
    }

    function addToPlonkList(user,alias){
      let bg = document.createElement("div");
      bg.className = "overlaybg";
      document.body.appendChild(bg);
      let win = document.createElement("div");
      win.className = "overlaywin";
      let form = document.createElement("form");
      form.setAttribute("id","plonkForm");
      buildForm(form,user,alias);
      win.appendChild(form);
      document.body.appendChild(win);
      return false;
    }

    function plonkContents(type){
      let contents;
      switch(type){
      case 'article': contents = document.getElementsByTagName(type); break;
      case 'comment': contents = document.getElementsByClassName(type); break;
      }
      for (let i = 0; i < contents.length; i++) {
        if(contents[i].getAttribute('class')==='poll-notice'){
          continue;
        }
        let userLink = contents[i].getElementsByClassName('meta').item(0).getElementsByTagName('a').item(0);
        let span = document.createElement("span");
        let sup = document.createElement("sup");
        let a = document.createElement("a");
        let txt = document.createTextNode(" Plonk");
        a.appendChild(txt);
        a.addEventListener("click",
          addToPlonkList.bind(null,userLink.href.match(userRE)[1],userLink.textContent),
          true);
        a.href = "#";
        sup.appendChild(a);
        span.appendChild(sup);
        userLink.parentNode.insertBefore(span,userLink.nextSibling);
        if(plonkClosedAccounts &&
          userLink.getAttribute('href') != "/users/collectif" &&
          userLink.getAttribute('href') != "/users/anonyme"){
          GM_xmlhttpRequest({
              method: "GET",
              url: userLink.href,
              context: contents[i],
              onload: function(response) {
                if(response.responseText.match(/Compte fermé/)){
                  response.context.style.display = "none";
                  if(unlistClosedAccounts){
                    let userLink =response.context.getElementsByClassName('meta').item(0).getElementsByTagName('a').item(0);
                    let user= userLink.href.match(userRE);
                    if(plonkList.hasOwnProperty(user)){
                      delete plonkList[user];
                      GM_setValue("plonkList",JSON.stringify(plonkList));
                    }
                  }
                }
              }
          });
        }
        let alteredList = false;
        for(let u in plonkList){
          if (userLink.href !== "https://linuxfr.org/users/"+u){
            continue;
          }
          if (userLink.textContent.toLowerCase() !== u && userLink.textContent !== plonkList[u][0] ){
            plonkList[u][0] = userLink.textContent;
            alteredList = true;
          }
          if(plonkList[u][types[type]]){
            contents[i].style.display = "none";
            break;
          }
        }
        if(alteredList){
          GM_setValue("plonkList",JSON.stringify(plonkList));
        }
      }
    }

    plonkContents('article');
    plonkContents('comment');

})();
