# DLFPlonk

## Qu'est ce que DLFPlonk ?

UserScript pour ignorer les contributions de certains utilisateurs du site
linuxfr.org

## Comment l'utiliser ?

### La méthode simple

Cliquez [ici](https://framagit.org/peetah/DLFPlonk/raw/master/DLFPlonk.user.js),
pour l'installer dans un navigateur compatible avec les UserScripts (par exemple,
en utilisant l'extension Violentmonkey, ou GreaseMonkey 3.XX).

### La méthode paranoïaque

Si le lien ci-dessus ne vous inspire pas confiance:

1. allez dans la section "Files" de ce dépôt gitlab

2. sélectionnez le fichier DLFPlonk.user.js

3. vérifiez que le code fait bien ce qui est présenté ici

4. cliquez sur le bouton "Raw" dans la barre de boutons en haut à droite pour l'installer

Le script devrait être installé et automatiquement activé lors de vos visites de
linuxfr.org
